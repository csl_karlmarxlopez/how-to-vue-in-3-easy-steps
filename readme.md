# How to install on existing project

## Requirements
node.js

> Run node -v to check if there is an existing installation

## Step 1
Install packages

Copy and paste this into your `package.json` `devDependencies`
```json
{
    "devDependencies": {
        "laravel-elixir": "^6.0.0-18",
        "laravel-elixir-vue-2": "^0.3.0",
        "laravel-elixir-webpack-official": "^1.0.10",
        "vue": "^2.6.10"
    }
}
```
Then, run `npm install`.

## Step 2
Add this to your `gulpfile.js` file
```js
//add this below existing requires
require('laravel-elixir-vue-2');


elixir(function(mix) {
    // add this at the end
    mix.webpack('app.js'); // app.js points to resources/assets/js/app.js
});
```

## Step 3
Create `app.js` inside `resources/assets/js`

Copy and paste this
```js
const app = new Vue({
  el: "#app", 
  data: {
      hello: 'Hello World'
  }
});
```
On your blade template add this
```handlebars
<div id="app>
    {{ hello }}
</div>
```

That's it! I hope I have changed your Vue.

Cheers!