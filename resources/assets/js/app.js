import Vue from "vue";

Vue.component("hello", require("./Hello.vue"));

const app = new Vue({
  el: "#app", 
  data: {
      hello: 'Hello World'
  }
});
